
-------------------------------------------------------------------------------
Icon Links for Drupal 7.x
  Original Icons Artwork by Subrat Nayak

	CC Some rights reserved. This work is licensed under a Creative Commons Attribution-Noncommercial-No Derivative Works 3.0 License.

	You are free to use these icons on any site you wish.

	You may NOT however sale, resale, repackage or in anyway profit from the original icon artwork provide for use with the icon links module.

  For more information contact info@pru8.com 

  Module Develped by Miles France, Pinnakl Web Solutions - miles.france (at) pinnakl (dot) com

-------------------------------------------------------------------------------

DESCRIPTION:
Icon links is a handy little module that allows you choose from a set of icons and set links for them in a standard drupal block.

-------------------------------------------------------------------------------

INSTALLATION:
* Put the module in your drupal modules directory and enable it in 
  admin/modules. 
* Configure the links at /admin/config/system/icon_links
* Configure the block at /admin/structure/block/manage/icon_links/icon-links/configure

-------------------------------------------------------------------------------


